package com.sanjeev.sn;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.graphics.Color;
import android.os.Bundle;


public class AddPostActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);


        Toolbar add_post_toolbar = findViewById(R.id.add_post_toolbar);
        setSupportActionBar(add_post_toolbar);

        this.getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setTitle("Add Post");


//        add_post_toolbar.setTitle("Sanjeev");
//        add_post_toolbar.setTitleTextColor(Color.parseColor("#000000"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}