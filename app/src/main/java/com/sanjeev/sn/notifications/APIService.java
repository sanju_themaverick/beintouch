package com.sanjeev.sn.notifications;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {

    @Headers({
            "Content-Type:application/json",
            "Authorization:key="


    })

    @POST("fcm/send")
    Call<Response> sendNotifications(@Body Sender body);
}
